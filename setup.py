import setuptools
from datetime import datetime
version = str(datetime.today()).split('.')[0].replace(
    '-', '.').replace(':', '.').replace(' ', '.')


def get_date():
    year, week = datetime.today().strftime("%Y %W").split(" ")
    return year, week

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read().replace("this_week", get_date()[1]).replace(
        "this_year", get_date()[0]).replace('__version__', version)
setuptools.setup(
    name="zermelo-for-python",
    version=version,
    author="b.vill",
    author_email="villeriusborro@gmail.com",
    description="zermelo api for python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'datetime',
        'requests'
    ],
    url="https://gitlab.com/btpv/zermelo_for_python",
    keywords=["zermelo","api"],

    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.0",
)
