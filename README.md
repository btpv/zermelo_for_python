# zermelo api for python v:__version__
![GitHub Logo](https://zermelo.nl/static/images/nav/zermelo.8ee5db22a007.svg)

## install

```properties
pip install zermelo-for-python
```
# [source code](https://gitlab.com/btpv/zermelo_for_python)
