#!/bin/sh
# cp "./src/zermeloforpython/__init__.py" "src\zermeloforpython\__init__.py"
# python3 -c "exec('path = \'src/zermeloforpython.egg-info\'\nfrom os import listdir,system\nfrom os.path import join\nitems = []\nfor i in listdir(path):\n\titems.append([join(path,i),join(path,i).replace(\'/\',\'\\\\\\\\\\\\\\\\\')])\nfor i in items:\n\tsystem(f\'cp {i[0]} {i[1]}\')')"
VERS=`./bin/python3 -c 'exec("import datetime\nprint(datetime.datetime.now())")'`
VERS="build $VERS"
echo "$VERS"
./bin/python3 -m build -s
./bin/python3 -m build -w
./bin/python3 -m twine upload dist/*
rm dist -r
rm build -r
git add .
git commit -m "$VERS" 
git push -u origin main
sleep 5
.bin/pip install --upgrade zermelo-for-python
